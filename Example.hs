{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
import           Control.Arrow
import           Control.Concurrent.STM
import           Control.Lens
import           Data.Aeson             (encode)
import qualified Data.ByteString.Lazy   as B
import           Data.Coerce
import           Git                    (CommitListSizeHint (..), gitInit, GitSort( .. ))
import           Hoardings
import qualified Git

data Person = Person
  { _personName  :: !String
  , _personEmail :: !(String, String)
  , _personAge   :: !Int
  } deriving (Show, Eq)

makeLenses ''Person

instance Stored Person where
  type Fields Person =
    '[ "personName" ::: String
     , "personEmail" ::: (String, String)
     , "personAge" ::: Int
     ]

  type Prev Person = 'Nothing

  fallback = Just Person
    { _personName = "John"
    , _personEmail = ("john", "gmail.com")
    , _personAge = 100
    }

  migrate = First

  format
    = field personName
    $ field personEmail
    $ field personAge
    FN

{-# SPECIALISE commitStore :: Store Person -> IO (Maybe Commit) #-}

self :: GitSignature
self = GitSignature "Mike Ledger" "michael.ledger@ricardo.com" Nothing

main :: IO ()
main = do
  () <- putStrLn "starting up!"

  let self = GitSignature
        { name = "google-uid:10859009638110749634"
        , email = "1@respecify.com"
        , when = Nothing
        }

  gitInit
  
  sig <- Git.newGitSignature self
  
  putStrLn "git initialised..."

  s <- newStore "person" self Person
    { _personName = "John"
    , _personEmail = ("john", "gmail.com")
    , _personAge = 100
    }

  let printStore = do
        B.putStr . encode . StoreJson =<< readStoreIO s
        putChar '\n'

  let printCommitStore = do
        putStrLn "committing..."
        commitStore s
        c <- currentCommit s
        putStrLn $ "new HEAD = " ++ show c

  putStrLn "created store..."
  printStore

  putStrLn "writing a new value to the store..."
  atomically (writeStore s self (Person "George" ("georeg", "gmail.com") 200))
  printCommitStore
  printStore

  putStrLn "modifying the store..."
  atomically (modifyStore s self (personName %~ \oldName -> oldName ++ " the Fifth"))
  printCommitStore
  printStore

  putStrLn "look at the commit list"
  print =<< commits s (Just 10) (Nothing @Commit)



