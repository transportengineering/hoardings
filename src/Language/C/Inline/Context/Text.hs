{-# LANGUAGE TemplateHaskell #-}
module Language.C.Inline.Context.Text (utf8Ctx) where
import qualified Data.ByteString                     as B
import qualified Data.Map.Strict                     as Map
import           Data.Text                           (Text, pack)
import qualified Data.Text.Encoding                  as T
import           Foreign
import           Foreign.C
import           Language.C.Inline.Context
import           Language.C.Inline.HaskellIdentifier
import           Language.C.Inline.Util
import qualified Language.C.Types                    as C

utf8Ctx :: Context
utf8Ctx = mempty
  { ctxAntiQuoters = Map.fromList
    [ ("utf8", SomeAntiQuoter utf8PtrAntiQuoter)
    , ("utf8[]", SomeAntiQuoter utf8StringPtrAntiQuoter)
    ]
  }

withUtf8String :: String -> (CString -> IO a) -> IO a
withUtf8String t = B.useAsCString (T.encodeUtf8 (pack t))

withUtf8 :: Text -> (CString -> IO a) -> IO a
withUtf8 t = B.useAsCString (T.encodeUtf8 t)

utf8PtrAntiQuoter :: AntiQuoter HaskellIdentifier
utf8PtrAntiQuoter = AntiQuoter
  { aqParser = do
      hId <- C.parseIdentifier
      let cId = mangleHaskellIdentifier True hId
      return (cId, C.Ptr [] (C.TypeSpecifier mempty (C.Char Nothing)), hId)

  , aqMarshaller = \_purity _cTypes cTy cId ->
      case cTy of
        C.Ptr _ (C.TypeSpecifier _ (C.Char Nothing)) -> do
          hsTy <- [t| Ptr CChar |]
          hsExp' <- [| withUtf8 $(getHsVariable "utf8Ctx" cId) |]
          return (hsTy, hsExp')
        _ ->
          fail "impossible: got type different from `char *' (utf8Ctx)"
  }


utf8StringPtrAntiQuoter :: AntiQuoter HaskellIdentifier
utf8StringPtrAntiQuoter = AntiQuoter
  { aqParser = do
      hId <- C.parseIdentifier
      let cId = mangleHaskellIdentifier True hId
      return (cId, C.Ptr [] (C.TypeSpecifier mempty (C.Char Nothing)), hId)

  , aqMarshaller = \_purity _cTypes cTy cId ->
      case cTy of
        C.Ptr _ (C.TypeSpecifier _ (C.Char Nothing)) -> do
          hsTy <- [t| Ptr CChar |]
          hsExp' <- [| withUtf8String $(getHsVariable "utf8Ctx" cId) |]
          return (hsTy, hsExp')
        _ ->
          fail "impossible: got type different from `char *' (utf8Ctx)"
  }

