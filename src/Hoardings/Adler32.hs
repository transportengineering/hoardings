{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Hoardings.Adler32 where

import Control.Lens
import Control.Monad.ST.Strict as ST
import Control.Monad.State.Strict
import Data.Bits
import qualified Data.ByteString as B
import qualified Data.ByteString.Short as BS
import qualified Data.ByteString.Unsafe as B
import Data.Functor.Identity
import Data.List (unfoldr)
import Data.Monoid (Endo (..))
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Unboxed as VU
import Data.Word
import Foreign.Storable
import GHC.Generics (Generic)
import Math.NumberTheory.Logarithms (integerLog2)

-- import Optics
-- import Optics.Label
-- import Optics.State.Operators

-- | Largest prime snmaller than 65536
base :: Word32
base = 65521

-- | nmax is the largest n such that 255n(n+1)/2 + (n+1)(base-1) <= 2^32-1
nmax :: Word
nmax = 5552

data RollingAdler32 = RollingAdler32 {a, b :: Word32}
  deriving (Eq, Ord, Show, Generic)

makeLensesFor [("a", "ra"), ("b", "rb")] ''RollingAdler32

type Adler32Update m = StateT RollingAdler32 m ()

type Stream m = m (Maybe Word8)

class Word8Buffer a where
  bufLength :: a -> Int
  bufUnsafeSlice :: Int -> Int -> a -> a
  bufUnsafeIndex :: a -> Int -> Word8
  bufUnsafeHead :: a -> Word8
  bufUnsafeHead = flip bufUnsafeIndex 0
  bufMapM_ :: Monad m => (Word8 -> m ()) -> a -> m ()

-- TODO instance for Integer?

instance x ~ Word8 => Word8Buffer (VS.Vector x) where
  bufLength = VS.length
  bufUnsafeSlice = VS.unsafeSlice
  bufUnsafeIndex = VS.unsafeIndex
  bufUnsafeHead = VS.unsafeHead
  bufMapM_ = VS.mapM_

instance x ~ Word8 => Word8Buffer (VU.Vector x) where
  bufLength = VU.length
  bufUnsafeSlice = VU.unsafeSlice
  bufUnsafeIndex = VU.unsafeIndex
  bufUnsafeHead = VU.unsafeHead
  bufMapM_ = VU.mapM_

instance Word8Buffer B.ByteString where
  bufLength = B.length
  bufUnsafeSlice start len = B.unsafeTake len . B.unsafeDrop start
  bufUnsafeIndex = B.unsafeIndex
  bufUnsafeHead = B.unsafeHead
  bufMapM_ f = B.foldr (\byte acc -> acc >> f byte) (pure ())

--
-- valid with bytestring >= 0.11.3.0
--
-- instance Word8Buffer BS.ShortByteString where
--   bufLength = BS.length
--   bufUnsafeSlice start len = BS.take len . BS.drop start
--   bufUnsafeIndex = B.unsafeIndex
--   bufUnsafeHead = B.unsafeHead
-- bufMapM_ f = B.foldr (\byte acc -> acc >> f byte) (pure ())
--

create :: Word32 -> RollingAdler32
create initialValue =
  RollingAdler32
    { a = initialValue .&. 0xFFFF,
      b = initialValue `shiftR` 16
    }

runM :: Monad m => Adler32Update m -> m RollingAdler32
runM f = f `execStateT` create 0

run :: Adler32Update Identity -> RollingAdler32
run = runIdentity . runM

runST :: (forall s. Adler32Update (ST s)) -> RollingAdler32
runST f = ST.runST (runM f)

feedSlow :: (Monad m, Word8Buffer buf) => buf -> Adler32Update m
feedSlow = bufMapM_ feedWord8

feedBuffer :: (Monad m, Word8Buffer buf) => buf -> Adler32Update m
feedBuffer buf = case bufLength buf of
  0 -> pure ()
  1 -> feedWord8 (buf `bufUnsafeIndex` 0)
  n | n < 16 -> feedSmallBuffer buf
  n -> feedLargeBuffer buf

{-# INLINE feedWord8 #-}
feedWord8 :: (Monad m) => Word8 -> Adler32Update m
feedWord8 byte = StateT \RollingAdler32 {a = a0, b = b0} -> do
  let w = fromIntegral byte :: Word32
  let a1 = (a0 + w) `rem` base
  let b1 = (b0 + a1) `rem` base
  pure ((), RollingAdler32 {a = a1, b = b1})

{-# INLINE feedWord16 #-}
feedWord16 :: Monad m => Word16 -> Adler32Update m
feedWord16 w = do
  feedWord8 (fromIntegral w)
  feedWord8 (fromIntegral (w `shiftR` 8))

{-# INLINE feedWord32 #-}
feedWord32 :: Monad m => Word32 -> Adler32Update m
feedWord32 w = do
  feedWord16 (fromIntegral w)
  feedWord16 (fromIntegral (w `shiftR` 16))

{-# INLINE feedWord64 #-}
feedWord64 :: Monad m => Word64 -> Adler32Update m
feedWord64 w = do
  feedWord32 (fromIntegral w)
  feedWord32 (fromIntegral (w `shiftR` 32))

feedSmallBuffer :: (Monad m, Word8Buffer buf) => buf -> Adler32Update m
feedSmallBuffer buf = do
  flip bufMapM_ buf \byte -> do
    ra += fromIntegral byte
    a <- use ra
    rb += a
  a <- use ra
  when (a >= base) do
    ra -= base
  rb %= (`rem` base)

-- See https://github.com/remram44/adler32-rs/blob/3ab7df27540f9a9cbd9d6270067d7070dfd18ff4/src/lib.rs#L140
feedLargeBuffer :: forall buf m. (Word8Buffer buf, Monad m) => buf -> Adler32Update m
feedLargeBuffer buf = do
  let len = fromIntegral (bufLength buf) :: Word
  let feedBlocks pos
        | pos + nmax <= len = do
          feedBlock pos (pos + nmax)
          ra %= (`rem` base)
          rb %= (`rem` base)
          pure pos
        | otherwise = pure pos
      feedBlock pos end
        | pos < end = do
          unsafe_do16 (bufUnsafeSlice (wordToInt pos) 16 buf)
          feedBlock (pos + 16) end
        | otherwise = pure pos
      feedRemainingBlocks pos
        | len - pos >= 16 = do
          unsafe_do16 (bufUnsafeSlice (wordToInt pos) 16 buf)
          feedRemainingBlocks (pos + 16)
        | otherwise = pure pos
      feedFinalWord32s pos
        | len - pos > 0 = do
          next <- state \RollingAdler32 {a = a0, b = b0} -> do
            let w = fromIntegral (buf `bufUnsafeIndex` wordToInt pos)
            let a1 = a0 + w
            let b1 = b0 + a1
            (pos + 1, RollingAdler32 {a = a1, b = b1})
          feedFinalWord32s next
        | otherwise = pure pos
  pos1 <- feedBlocks 0
  when (pos1 < len) do
    feedRemainingBlocks pos1 >>= feedFinalWord32s
    ra %= (`rem` base)
    rb %= (`rem` base)

feedInteger :: Monad m => Integer -> Adler32Update m
feedInteger 0 = feedWord8 0
feedInteger n
  | n >= fromIntegral (maxBound :: Int) && n <= fromIntegral (maxBound :: Int) =
    feedInt (fromIntegral n)
feedInteger n =
  VU.mapM_ feedWord32 (VU.replicateM size step `evalState` n)
  where
    size = integerLog2 (abs n) `div` 32
    step = state \i -> (fromIntegral i, i `shiftR` 32)

isWordWord64, isWordWord32 :: Bool
isWordWord64 = finiteBitSize (0 :: Word64) == finiteBitSize (0 :: Word)
isWordWord32 = finiteBitSize (0 :: Word32) == finiteBitSize (0 :: Word)

feedWord :: Monad m => Word -> Adler32Update m
feedWord
  | isWordWord64 = feedWord64 . fromIntegral
  | isWordWord32 = feedWord32 . fromIntegral
  | otherwise = error "unsupported word type"

feedInt :: Monad m => Int -> Adler32Update m
feedInt = feedWord . fromIntegral

wordToInt :: Word -> Int
wordToInt w = case fromIntegral w of
  i
    | i >= 0 -> i
    | otherwise -> error "fromWord: overflow"

--------------------------------------------------------------------------------

unsafe_do1,
  unsafe_do2,
  unsafe_do4,
  unsafe_do8,
  unsafe_do16 ::
    (Monad m, Word8Buffer buf) => buf -> Adler32Update m
unsafe_do1 buf = state \r ->
  let byte = bufUnsafeHead buf
      nextA = a r + fromIntegral byte
      nextB = b r + nextA
   in ((), RollingAdler32 {a = nextA, b = nextB})
unsafe_do2 buf = do
  unsafe_do1 (bufUnsafeSlice 0 1 buf)
  unsafe_do1 (bufUnsafeSlice 1 1 buf)
unsafe_do4 buf = do
  unsafe_do2 (bufUnsafeSlice 0 2 buf)
  unsafe_do2 (bufUnsafeSlice 2 2 buf)
unsafe_do8 buf = do
  unsafe_do4 (bufUnsafeSlice 0 4 buf)
  unsafe_do4 (bufUnsafeSlice 4 4 buf)
unsafe_do16 buf = do
  unsafe_do8 (bufUnsafeSlice 0 8 buf)
  unsafe_do8 (bufUnsafeSlice 8 8 buf)
{-# INLINE unsafe_do1 #-}
{-# INLINE unsafe_do2 #-}
{-# INLINE unsafe_do4 #-}
{-# INLINE unsafe_do8 #-}
{-# INLINE unsafe_do16 #-}

{-# INLINE streamVector #-}
streamVector :: MonadState Int m => VU.Vector Word8 -> m (Maybe Word8)
streamVector buf = state \i -> (buf VU.!? i, i + 1)
