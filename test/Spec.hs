module Spec where

import qualified Adler32
import Test.Tasty (defaultMain, testGroup)

main :: IO ()
main = defaultMain (testGroup "hoardings-test" [Adler32.tests])
