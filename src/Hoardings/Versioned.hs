{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Hoardings.Versioned
  ( Versioned (..),
    Checksummed (..),
    versioned,

    -- * Classes
    HasVersion (..),
    HasItem (..),

    -- * Version type
    Version,
    incrementVersion,
  )
where

import Control.Lens
import Data.Bits (xor)
import Data.Default
import Data.Kind (Constraint)
import Data.Word
import GHC.Generics (Generic)
import qualified Hoardings.Adler32 as A32
import Hoardings.Checksum

newtype Version = Version Word64
  deriving (Eq, Show, Ord)

instance Semigroup Version where
  Version a <> Version b = Version (checksumToWord64 (checksum (a, b)))

instance Monoid Version where
  mempty = Version 0

combine :: Versioned a -> Versioned b -> Versioned (a, b)
combine (Versioned av a) (Versioned bv b) = Versioned (av <> bv) (a, b)

incrementVersion :: Version -> Version
incrementVersion (Version a) = Version (a + 1)

data Versioned a = Versioned
  { _verVersion :: Version,
    _verItem :: ~a
  }
  deriving (Eq, Show, Generic)

data Checksummed a = Checksummed
  { _csVersion :: Version,
    _csChecksum :: ~Checksum,
    _csItem :: ~a
  }
  deriving (Eq, Show, Generic)

makeLenses ''Versioned
makeLenses ''Checksummed

class HasVersion a where
  touch :: a -> a
  getVersion :: a -> Version
  hasUpdated :: a -> a -> Bool

instance HasVersion (Checksummed a) where
  getVersion = _csVersion
  touch a = a {_csVersion = incrementVersion (_csVersion a)}
  hasUpdated a b = _csVersion a /= _csVersion b || _csChecksum a /= _csChecksum b

instance HasVersion (Versioned a) where
  getVersion = _verVersion
  touch a = a {_verVersion = incrementVersion (_verVersion a)}
  hasUpdated a b = _verVersion a /= _verVersion b

class HasItem s a | s -> a where
  item :: Lens' s a

instance Checksumable a => HasItem (Checksummed a) a where
  item = lens _csItem \v a ->
    v
      { _csChecksum = checksum a,
        _csVersion = _csVersion v,
        _csItem = a
      }

instance HasItem (Versioned a) a where
  item = lens _verItem \v a ->
    v
      { _verVersion = _verVersion v,
        _verItem = a
      }

versioned :: a -> Versioned a
versioned = Versioned mempty

checksummed :: Checksumable a => a -> Checksummed a
checksummed a = Checksummed mempty (checksum a) a
