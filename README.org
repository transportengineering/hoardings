* ANN: Hoardings: a git-backed document storage library for Haskell

`hoardings` is a library developed for storing documents in a Git repository. It
is fairly simple why you might want to do this; if you have a text-based
document type that you want to be able to track changes to, then you may as well
use a proven and battle-tested tool like Git to do it. Git provides some other
benefits as well, such as its resilience to corruption, ease of backups,
branching, etc.

The core of `hoardings` are small and limited Haskell bindings to `libgit2`.
This is the library used by websites such as GitHub and GitLab to view and
manage Git repositories. Strangely, `libgit2` is not the library that underpins
the `git` command-line tool itself, though they both use the same format. (That
might change, someday, but for now they remain separate.)

Hoardings boasts a simple and fast serialisation mechanism that can be applied
to existing Haskell types without much fuss. It also supports type-safe
migrations from different versions of types. Files are currently written into
Yaml as it is a heavily "line-based" format (and hence lends itself to nice Git
diffs), though I plan to add support for writing `Text` as plain-text as well.

** Usage

A type supported by `hoardings` must have an instance of `Hoardings.Stored`.
These allow you to specify the subset of your document type that is written to
Git, and how this should be achieved. At it's core, it's a list of fields, the
lenses into them, a default value (if desired), and a function that migrates
from the previous version of the type to the current one. For instance, a simple
word processor might use:

#+BEGIN_CODE
data Document = Document
  { _text :: Rope
  , _authors :: [Text]
  , _created :: UTCTime
  , _modified :: UTCTime
  }

makeLenses ''Document

instance Stored Document where
  type Fields Document =
   '[ "text" ::: Text
    , "authors" ::: [Text]
    , "created" ::: UTCTime
    , "modified" ::: UTCTime
    ]
  format 
    = field (text . ropeText) 
    $ field authors
    $ field created
    $ field modified FN
#+END_CODE

The rest of the API should be familiar if you have used `IORef` before;
`hoardings` provides a wrapper `Store` type that has a similar API to
`Data.IORef`; `newStore` creates a new store, `modifyStore` modifies it,
`writeStore` writes to it, etc. `hoardings` leaves the timing of actual Git
commits -- through `commitStore` -- up to the user, though it will not write
anything if changes have not actually made.

See the `Hoardings` module.

** Use of `inline-c`

These bindings are achieved through `inline-c`, which has been a pleasure to
use. Originally `libgit2` was used via https://github.com/jwiegley/gitlib, but
I've found directly writing C, via `inline-c` to be a much easier approach; it
matches the C API exactly, requires no marshalling for temporary/local variables
(which can also be allocated on the C stack as a bonus; all Haskell allocations
are on the heap, and make the usual pitfalls with use of pointers easier to run
into), and gives you access to variadic functions (like [[https://libgit2.github.com/libgit2/#HEAD/group/commit/git_commit_create_v][git_commit_create_v]])
that would have been inaccessible to the Haskell FFI. Having an actual C
compiler that can generate warnings about e.g. possibly unsafe pointer casts use
has also been extremely helpful.

In the bindings approach, even invoking a function that has a `const char*`
argument -- even if the string is unchanging across the lifetime of the program
-- is a relative ordeal. You must use `Data.ByteString.useAsCString` to allocate
the `CString`. This unfortunately adds massive clutter to code that distracts
from the intent of even basic code. e.g. compare

#+BEGIN_CODE
  ...
  commitOid <- allocaBytes 20 $ \coid -> -- hope that git_oid does not change size
    B.useAsCString "HEAD" $ \cupdateref ->
    B.useAsCString "UTF-8" $ \cencoding ->
    B.useAsCString "initial commit" $ \cmsg ->
    allocaGitSignature self $ \csig -> do
      c'git_commit_create coid repo cupdateref csig csig cencoding cmsg ctree 0 NULL
      B.packCStringLen 20 (castPtr coid) -- git_oid uses unsigned char so requires cast
  doSomethingWith commitOid
#+END_CODE

with

#+BEGIN_CODE
  ...
  git_oid oid;
  git_signature* sig = $git_signature:self;
  git_commit_create(&oid, $(git_repository* repo), "HEAD", sig, sig, "UTF-8", "initial commit", tree, 0);
  do_something_with(&oid);
#+END_CODE

Using `ContT` may yield a mild improvement to the Haskell code. To sum up, hats
off to the authors at FP Complete of `inline-c`; this is an incredibly useful
library.


