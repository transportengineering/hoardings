{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Hoardings.Storage
  ( -- * Core typeclass
    Stored (..),
    migratePure,
    Constructor (..),

    -- ** Format specifiers
    Format (..),
    type Size,
    field,
    fieldPath,
    type FieldLike,
    Field (..),
    FieldPath (..),
    type (:::),

    -- ** Migrations
    Migration (..),
    type Version,

    -- * Combinators
    withVersion,
    withFormat,
    migrateFromTo,
    withFields,
    readFieldFiles,
    setFieldsFrom,
    construct,

    -- * Internals
    Fld (..),
    MapFields (mapFields),
  )
where

import Control.Lens
  ( ALens',
    assign,
    cloneLens,
    set,
    view,
  )
import Control.Monad.Except
import Control.Monad.State.Strict
import Data.Aeson
  ( FromJSON,
    ToJSON,
  )
import Data.Kind
import Data.Proxy
import Data.Type.Equality
import qualified Data.Yaml as Yaml
import GHC.TypeLits
import Type.Reflection

data Fld = Fld Symbol Type

type k ::: a = 'Fld k a

-- | Fields must satisfy these constraints
type FieldLike x = (Eq x, Typeable x, ToJSON x, FromJSON x)

newtype Field :: Symbol -> Type -> Type -> Type where
  Field :: {fieldLens :: ALens' s x} -> Field sym s x

newtype FieldPath = FieldPath {getFieldPath :: FilePath}
  deriving (Show, Eq, Ord)

fieldPath :: KnownSymbol path => Field path s x -> FieldPath
fieldPath (_ :: Field path s x) = FieldPath (symbolVal (Proxy :: Proxy path))

-- | Specifies the serialisable fields of a datatype. Construct a 'Format' by
-- chaining together 'field' like:
--
-- @
-- personFormat = 'field' name
--              $ 'field' email
--              $ 'field' age
--              $ 'FN'
-- @
data Format :: Type -> [Fld] -> Type where
  FC ::
    (KnownSymbol path, FieldLike x) =>
    Field path s x ->
    Format s xs ->
    Format s ((path ::: x) : xs)
  FN :: Format s '[]

{-# INLINE field #-}
field ::
  (KnownSymbol path, FieldLike x) =>
  ALens' s x ->
  Format s xs ->
  Format s ((path ::: x) : xs)
field = FC . Field

-- | A migration from a previous version of the stored datatype to a new one.
data Migration s where
  -- | Usually you will just need to map the fields of the old datatype onto the
  -- new one.
  MigrateIO :: (Prev s ~ 'Just x, Stored x) => (x -> IO s) -> Migration s
  -- | @deprecated@ use 'migratePure'
  Migrate :: (Prev s ~ 'Just x, Stored x) => (x -> s) -> Migration s
  -- | Use for the first version of your type
  First :: Prev s ~ 'Nothing => Migration s

-- | Pure migration
migratePure :: (Prev s ~ 'Just x, Stored x) => (x -> s) -> Migration s
migratePure f = MigrateIO (pure . f)

prev :: forall s x. Prev s ~ 'Just x => Proxy x
prev = Proxy

prevRep :: forall s x. (Prev s ~ 'Just x, Typeable x) => TypeRep x
prevRep = typeRep

--------------------------------------------------------------------------------

type family Construct (fields :: [Fld]) (s :: Type) :: Type where
  Construct ((k ::: x) : xs) r = x -> Construct xs r
  Construct '[] r = r

--------------------------------------------------------------------------------

newtype Constructor f s = Constructor
  {runConstructor :: Construct f s}

--------------------------------------------------------------------------------

class
  ( Typeable s,
    KnownNat (Version s),
    KnownNat (Size s),
    MapFields (Fields s)
  ) =>
  Stored s
  where
  type Prev s :: Maybe Type
  type Prev s = 'Nothing

  type Fields s :: [Fld]

  --  | Placeholder value for document, used when fields couldn't be read for
  -- some reason -- you might want to fill this with error values, or it to be
  -- undefined.
  fallback :: Maybe s
  fallback = Nothing

  constructor :: Monad m => Constructor (Fields s) (m s)
  constructor = case fallback :: Maybe s of
    Just v -> defaultConstruct v (format :: Format s (Fields s))
    Nothing -> error "defaultConstructor requires fallback value"

  --  | Migrate to this version
  migrate :: Migration s
  default migrate :: Prev s ~ 'Nothing => Migration s
  migrate = First

  --  | Describes the fields available for this document according to its 'Fields'
  format :: Format s (Fields s)

type family Size s where
  Size s = S1 0 (Fields s)

type family S1 acc l where
  S1 acc (x : xs) = S1 (1 + acc) xs
  S1 acc '[] = acc

type family Version x where
  Version x = V1 0 (Prev x)

type family V1 acc s :: Nat where
  V1 acc ('Just x) = V1 (1 + acc) (Prev x)
  V1 acc 'Nothing = acc

{-# INLINE withFormat #-}
withFormat ::
  forall s m.
  (Stored s, Monad m) =>
  (forall x. FieldLike x => FieldPath -> ALens' s x -> m ()) ->
  m ()
withFormat f = mapFieldsM f (format :: Format s (Fields s))

{-# INLINE withFields #-}
withFields ::
  (Stored s, Monad m) =>
  (forall x. FieldLike x => FieldPath -> x -> m ()) ->
  s ->
  m ()
withFields f s = withFormat (\xp xl -> f xp (view (cloneLens xl) s))

{-# INLINE setFieldsFrom #-}
setFieldsFrom ::
  (Stored s, Monad m) =>
  (forall x. FieldLike x => FieldPath -> x -> m x) ->
  s ->
  m s
setFieldsFrom f s = (`execStateT` s) $
  withFormat $ \fp l -> do
    r <- lift (f fp (view (cloneLens l) s))
    assign (cloneLens l) r

{-# INLINE readFieldFiles #-}
readFieldFiles ::
  forall s.
  Stored s =>
  (FieldPath -> FilePath) ->
  IO (Either Yaml.ParseException s)
readFieldFiles fld = runExceptT $
  construct $ \fp l ->
    liftIO (Yaml.decodeFileEither (fld fp)) >>= \case
      Left er -> case fallback @s of
        Just v -> return (view (cloneLens l) v)
        Nothing -> throwError er
      Right v -> return v

{-# INLINE construct #-}
construct ::
  (Monad m, Stored s) =>
  (forall x. FieldLike x => FieldPath -> ALens' s x -> m x) ->
  m s
construct f = constructFrom f constructor format

--------------------------------------------------------------------------------
-- Migrations

{-# INLINE withVersion #-}

-- | Retrieve the 'Stored' dictionary for a specific, possibly previous,
-- version
withVersion ::
  forall proxy s b.
  Stored s =>
  -- | Current version
  proxy s ->
  -- | Version to find
  Integer ->
  -- | Function to apply on previous version
  (forall x. Stored x => TypeRep x -> b) ->
  b ->
  b
withVersion _ wanted f z
  | cur == wanted = f (typeRep :: TypeRep s)
  | otherwise = case migrate :: Migration s of
    Migrate _ -> withVersion (prev @s) wanted f z
    MigrateIO _ -> withVersion (prev @s) wanted f z
    First -> z
  where
    cur = natVal (Proxy :: Proxy (Version s))

{-# INLINE migrateFromTo #-}
migrateFromTo ::
  forall s0 s1.
  (Stored s1, Typeable s1) =>
  TypeRep s0 ->
  TypeRep s1 ->
  s0 ->
  IO s1
migrateFromTo pfrom pto = case testEquality pfrom pto of
  Just Refl -> pure
  Nothing -> case migrate @s1 of
    MigrateIO f -> f <=< migrateFromTo pfrom (prevRep @s1)
    Migrate f -> (pure . f) <=< migrateFromTo pfrom (prevRep @s1)
    First -> error ("no migration found: " ++ show (pfrom, pto))

--------------------------------------------------------------------------------
-- Dark spooky internals
--
-- By iterating through the list of fields at the type level, GHC is able to
-- inline each step of the recursion, thus enabling a host of other
-- optimisations, as well as being faster in its own right.
--
-- The risk is that in if somehow 'MapFields' *isn't* inlined (or GHC just
-- doesn't apply the optimisation due to -O0 or -O1), then it's probably going
-- to be slower than the ordinary recursive version of 'mapFieldsM'; it
-- certainly won't be faster.
--

class MapFields (l :: [Fld]) where
  mapFields ::
    (forall x. FieldLike x => FieldPath -> ALens' s x -> ALens' s' x) ->
    Format s l ->
    Format s' l

  mapFieldsM :: -- XXX this should be called mapFieldsM_
    Monad m =>
    (forall x. FieldLike x => FieldPath -> ALens' s x -> m ()) ->
    Format s l ->
    m ()

  constructFrom ::
    Monad m =>
    (forall x. FieldLike x => FieldPath -> ALens' s x -> m x) ->
    Constructor l (m s) ->
    Format s l ->
    m s

  defaultConstruct :: Monad m => s -> Format s l -> Constructor l (m s)

instance MapFields '[] where
  {-# INLINE mapFields #-}
  {-# INLINE mapFieldsM #-}
  {-# INLINE constructFrom #-}
  {-# INLINE defaultConstruct #-}
  mapFields _ _ = FN
  mapFieldsM _ _ = return ()
  constructFrom _ c _ = runConstructor c
  defaultConstruct s _ = Constructor (return s)

instance
  (KnownSymbol k, FieldLike x, MapFields xs) =>
  MapFields ((k ::: x) : xs)
  where
  {-# INLINE mapFields #-}
  {-# INLINE mapFieldsM #-}
  {-# INLINE constructFrom #-}
  {-# INLINE defaultConstruct #-}
  mapFields f (FC x xs) =
    FC (Field (f (fieldPath x) (fieldLens x))) (mapFields f xs)

  mapFieldsM f (FC x xs) = do
    f (fieldPath x) (fieldLens x)
    mapFieldsM f xs

  constructFrom fld f (FC xf xs) = do
    x <- fld (fieldPath xf) (fieldLens xf)
    constructFrom fld (appConstr f x) xs

  defaultConstruct ::
    forall m s.
    Monad m =>
    s ->
    Format s ((k ::: x) : xs) ->
    Constructor ((k ::: x) : xs) (m s)
  defaultConstruct s (FC x xs) = Constructor $ \v ->
    let c :: Constructor xs (m s)
        c = defaultConstruct (set (cloneLens (fieldLens x)) v s) xs
     in runConstructor c

{-# INLINE appConstr #-}
appConstr :: Constructor ((k ::: x) : xs) s -> x -> Constructor xs s
appConstr (Constructor f) x = Constructor (f x)
