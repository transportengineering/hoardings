{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- | Provides git-based yaml file storage.
--
-- Your document datatype must be an instance of the 'Stored' class, which
-- also enables migrations between versions of the type.
module Hoardings
  ( -- * Core typeclass
    module Hoardings.Storage,

    -- * Stores
    Store (..),
    version,
    storeFilePath,
    storeVersionFile,
    storeVersionFileName,

    -- ** Construction
    newStore,

    -- ** Reading
    readStore,
    readStoreIO,

    -- ** Writing
    writeStore,
    writeStoreRepr,
    modifyStore,
    modifyStoreRepr,

    -- ** Commits
    GitSignature (..),
    Git.Commit (..),
    currentCommit,
    commitStore,

    -- ** Revisions
    getCommit,
    commits,
    oidToCommit,

    -- * Git utility
    HasGitOid,
    Git.GitOid (..),
    Git.GitOidHash (..),
    Git.GitCommitOid (..),
    Git.parseGitOidHash,
    Git.formatGitOid,

    -- * JSON helper
    StoreJson (..),
  )
where

import Control.Concurrent.STM
import Control.Exception (evaluate)
import Control.Lens (cloneLens, view)
import Control.Monad.State.Strict
import Data.Aeson hiding (Object)
import Data.Aeson.CompatObject
import Data.ByteString (ByteString)
import Data.Coerce
import qualified Data.HashMap.Strict as H
import Data.IORef
import Data.List (sortOn)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Ord (Down (..))
import Data.Proxy
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Read as T
import Data.Time (getCurrentTime)
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM
import qualified Data.Yaml as Yaml
import Foreign
import GHC.TypeLits
import Git
  ( GitRepository,
    GitSignature,
  )
import qualified Git
import Hoardings.Orphans ()
import Hoardings.Storage
import System.Directory
import System.FilePath
import Type.Reflection

data Store a = Store
  { stored :: {-# UNPACK #-} !(TVar a),
    previous :: {-# UNPACK #-} !(TVar a),
    -- | Changes made since the last commit
    changes :: {-# UNPACK #-} !(TVar (Map GitSignature Word)),
    -- | Changes made since the creation of the document
    --
    -- kept a track of totalChanges only because 'M.size' is /O(log n)/ and
    -- because 'commitMsg' wants the number of total changes for a commit. But
    -- is it actually worth it? I doubt it makes a difference either way, except
    -- possibly slowing down 'modifyStore'...
    totalChanges :: {-# UNPACK #-} !(TVar Word),
    lastCommit :: {-# UNPACK #-} !(TVar Git.Commit),
    storeGit :: {-# UNPACK #-} !(TMVar (ForeignPtr GitRepository)),
    rootPath :: !FilePath
  }

version :: Stored a => store a -> Integer
version (_ :: store a) = natVal (Proxy :: Proxy (Version a))

storeFilePath :: FilePath -> FieldPath -> FilePath
storeFilePath rootp (FieldPath rel) = rootp </> rel <.> "yaml"

storeVersionFileName :: FilePath
storeVersionFileName = ".aeson-files-version.yaml"

storeVersionFile :: FilePath -> FilePath
storeVersionFile rootp = rootp </> storeVersionFileName

--------------------------------------------------------------------------------

withStoreGit :: Store a -> (ForeignPtr GitRepository -> IO x) -> IO x
withStoreGit Store {storeGit} withfn = do
  pgit <- atomically (takeTMVar storeGit)
  r <- withfn pgit
  atomically (putTMVar storeGit pgit)
  pure r

--------------------------------------------------------------------------------
-- Initialisation

readVersionFile :: FilePath -> IO Integer
readVersionFile root =
  Yaml.decodeFileEither (storeVersionFile root) >>= \case
    Left err -> error ("could not read version file!\n" ++ show err)
    Right x -> return x

-- | Create or load a store from a given root directory. If the directory does
-- not exist, this creates it, along with a new git repository.
--
-- The special file given by @storeVersionFile@ (".aeson-files-version.yaml")
-- indicates the current schema for this store; this must not be modified by the
-- user.
newStore ::
  forall s. (Stored s) => FilePath -> GitSignature -> s -> IO (Store s)
newStore root owner val0 = do
  dirExists <- doesDirectoryExist root
  dirIsEmpty <- if dirExists then null <$> listDirectory root else pure True

  when
    (dirExists && dirIsEmpty)
    ( error $
        unlines
          [ "The directory " ++ show root ++ " already exists, but is empty",
            "Hoardings repositories are directories created through Git.",
            "Manually delete or move this directory."
          ]
    )

  pgit <- Git.newGitRepository root owner
  rev <- newTVarIO M.empty
  doMigration <- newIORef False
  val <-
    if dirExists && not dirIsEmpty
      then do
        let currentVersionProxy = typeRep :: TypeRep s
        actualDataVersion <- readVersionFile root
        withVersion
          currentVersionProxy
          actualDataVersion
          ( \(rep :: TypeRep x) -> do
              readResult <- readFieldFiles @x (storeFilePath root)
              case readResult of
                Right v -> do
                  !result <- evaluate =<< migrateFromTo rep (typeRep :: TypeRep s) v
                  writeIORef doMigration True
                  return result
                Left er -> error $ "could not read store: " ++ show er
          )
          (error ("unable to find version " ++ show actualDataVersion))
      else do
        let currentVersionNo = natVal (Proxy :: Proxy (Version s))
        -- write the schema version file and commit it
        Yaml.encodeFile (storeVersionFile root) currentVersionNo
        -- write the initial value to repo and commit
        commitPaths <- do
          !vm <- VM.new (1 + fromInteger (natVal (Proxy :: Proxy (Size s))))
          VM.write vm 0 (makeRelative root (storeVersionFile root))
          fileIndexRef <- newIORef (1 :: Int)
          !_ <- withFormat @s $ \xfp xl -> do
            i <- atomicModifyIORef' fileIndexRef (\i -> (i + 1, i))
            let abspath = storeFilePath root xfp
            let gitpath = makeRelative root abspath
            Yaml.encodeFile abspath (cloneLens xl `view` val0)
            VM.write vm i gitpath
          V.unsafeFreeze vm
        let msg = commitMsg 0 (Proxy :: Proxy s) [owner]
        !_commit <- Git.commit commitPaths pgit msg owner
        return val0
  ref <- newTVarIO val
  prev <- newTVarIO val
  hc <- Git.headCommit pgit
  tc <- newTVarIO (getChanges (Git.commitMessage hc))
  lc <- newTVarIO hc
  lockedGit <- newTMVarIO pgit
  let s =
        Store
          { stored = ref,
            previous = prev,
            changes = rev,
            totalChanges = tc,
            lastCommit = lc,
            storeGit = lockedGit,
            rootPath = root
          }
  needMigration <- readIORef doMigration
  if needMigration
    then do
      hsig <- migrationSig
      Just _commit <-
        commitStoreWithOpts
          s
          CommitOpts
            { forceCommitIfNoChanges = True,
              forcedAuthors = [hsig, owner],
              forceWriteVersion = True
            }
      return s
    else return s

migrationSig :: IO GitSignature
migrationSig = do
  now <- getCurrentTime
  pure
    Git.GitSignature
      { gitSigName = "hoardings/migration",
        gitSigEmail = "hoardings@not_a_real_email_address.com",
        gitSigWhen = Just now
      }

getChanges :: Text -> Word
getChanges t = case T.lines t of
  h : _ | [_, c] <- T.split (== '#') h, Right (v, _) <- T.decimal c -> v
  _ -> 0

--------------------------------------------------------------------------------
-- Modification

{-# INLINE writeStore #-}

-- | Make a modification to the store.
writeStore :: Store s -> GitSignature -> s -> STM ()
writeStore Store {stored, totalChanges, changes} sig x = do
  modifyTVar' changes (M.insertWith (+) sig 1)
  modifyTVar' totalChanges (+ 1)
  writeTVar stored x

{-# INLINE writeStoreRepr #-}

-- | Make a modification to the store that /won't/ be observed towards a commit.
-- This should only be used to change the value of a "computed" field, that does
-- not actually get serialised, or is only there as a cache for some expensive
-- computation.
writeStoreRepr :: Store s -> s -> STM ()
writeStoreRepr Store {stored} = writeTVar stored

{-# INLINE modifyStore #-}

-- | Make a modification to the store.
modifyStore :: Store s -> GitSignature -> (s -> s) -> STM ()
modifyStore Store {stored, totalChanges, changes} sig f = do
  modifyTVar' changes (M.insertWith (+) sig 1)
  modifyTVar' totalChanges (+ 1)
  modifyTVar' stored f

{-# INLINE modifyStoreRepr #-}

-- | Make a modification to the store that /won't/ be observed towards a commit.
-- This should only be used to change the value of a "computed" field, that does
-- not actually get serialised, or is only there as a cache for some expensive
-- computation.
modifyStoreRepr :: Store s -> (s -> s) -> STM ()
modifyStoreRepr Store {stored} = modifyTVar' stored

commitStore :: Stored s => Store s -> IO (Maybe Git.Commit)
commitStore s =
  commitStoreWithOpts
    s
    CommitOpts
      { forceCommitIfNoChanges = False,
        forcedAuthors = [],
        forceWriteVersion = False
      }

data CommitOpts = CommitOpts
  { forceCommitIfNoChanges :: Bool,
    forcedAuthors :: [GitSignature],
    forceWriteVersion :: Bool
  }

{-# INLINEABLE commitStoreWithOpts #-}

-- | Commit any changes to the store. Does nothing if there are no changes, or
-- if the changes did not actually modify the existing values.
--
-- Fields are individually checked through '==' to see whether they have changed
-- between revisions of the document -- this is good for "small" fields whose
-- values are cheap to compare, but you might want to wrap larger fields in
-- 'Data.Hashable.Cached.Lazy.Hashed'.
commitStoreWithOpts ::
  forall s. Stored s => Store s -> CommitOpts -> IO (Maybe Git.Commit)
commitStoreWithOpts s@Store {previous, stored, rootPath, changes, lastCommit, totalChanges} CommitOpts {forceCommitIfNoChanges, forcedAuthors, forceWriteVersion} =
  withStoreGit s $ \pgit -> do
    chs <- readTVarIO changes
    let changeCount = M.foldl' (+) 0 chs
    -- do nothing if there are no declared changes
    if not forceCommitIfNoChanges && changeCount <= 0
      then return Nothing
      else do
        (!prev, !cur, !tc) <- atomically $ do
          x0 <- readTVar previous
          x1 <- readTVar stored
          tc <- readTVar totalChanges
          writeTVar changes M.empty
          return (x0, x1, tc)
        modifiedPaths <- do
          !vm <- VM.new (fromInteger (natVal (Proxy :: Proxy (Size s))))
          !sz <- (`execStateT` (0 :: Int)) $
            withFormat @s $ \xfp xl -> do
              !i <- get
              -- for each field, check if the value actually changed
              let x1 = cloneLens xl `view` cur
              when (forceCommitIfNoChanges || (cloneLens xl `view` prev) /= x1) $ do
                liftIO $ do
                  let abspath = storeFilePath rootPath xfp
                  let gitpath = makeRelative rootPath abspath
                  Yaml.encodeFile abspath x1
                  VM.write vm i gitpath
                modify' (+ 1)
          V.unsafeTake sz <$> V.unsafeFreeze vm
        if not forceCommitIfNoChanges && null modifiedPaths
          then return Nothing
          else do
            let authors@(author : _) =
                  forcedAuthors ++ map fst (sortOn (Down . snd) (M.toList chs))
            when
              forceWriteVersion
              (Yaml.encodeFile (storeVersionFile rootPath) (version s))
            oid <-
              if forceWriteVersion
                then Git.commitAll pgit (commitMsg tc s authors) author
                else Git.commit modifiedPaths pgit (commitMsg tc s authors) author
            com <- Git.lookupCommit pgit oid
            atomically (writeTVar lastCommit com)
            return (Just com)

{-# INLINE readStore #-}

-- | Read the current value of the 'Store'
readStore :: Store s -> STM s
readStore s = readTVar (stored s)

{-# INLINE readStoreIO #-}

-- | Read the current value of the 'Store'. Uses 'readTVarIO' which is,
-- apparently, much faster than 'readTVar'
readStoreIO :: Store s -> IO s
readStoreIO s = readTVarIO (stored s)

--------------------------------------------------------------------------------
-- Reading previous values

class HasGitOid c where gitOid :: c -> IO Git.GitOid

instance HasGitOid Git.GitCommitOid where
  gitOid = return . coerce

instance HasGitOid Git.GitOid where
  gitOid = return

instance HasGitOid Git.GitOidHash where
  gitOid = Git.packGitOidHash

instance HasGitOid Git.Commit where
  gitOid = gitOid . Git.commitOid

data StoreFiles = StoreFiles
  { gotBlobs :: !(Map FieldPath ByteString),
    gotVersion :: !(Maybe Integer)
  }

currentCommit :: Store s -> IO Git.Commit
currentCommit = readTVarIO . lastCommit

typeRepOf :: Typeable a => proxy a -> TypeRep a
typeRepOf _ = typeRep

getCommit :: (HasGitOid c, Stored s) => Store s -> c -> IO s
getCommit store oidish = do
  oid <- gitOid oidish
  StoreFiles {gotBlobs, gotVersion} <- withStoreGit store $ \pgit ->
    Git.foldCommit
      oid
      pgit
      ( \fp bs sf ->
          if fp == storeVersionFileName
            then case Yaml.decodeEither' bs of
              Right v -> return sf {gotVersion = Just v}
              Left err -> error $ "Could not parse version, got error " ++ show err
            else case splitExtension fp of
              (name, ".yaml") ->
                return sf {gotBlobs = M.insert (FieldPath name) bs (gotBlobs sf)}
              _ -> return sf
      )
      StoreFiles {gotBlobs = M.empty, gotVersion = Nothing}
  case gotVersion of
    Just cur ->
      withVersion
        store
        cur
        ( \rep ->
            migrateFromTo rep (typeRepOf store)
              =<< construct
                ( \fp l -> do
                    let fldValue = M.lookup fp gotBlobs
                    case fldValue of
                      Nothing
                        | Just fallbackValue <- fmap (cloneLens l `view`) fallback ->
                          pure fallbackValue
                      Nothing ->
                        error
                          "Field is not available in store files, nor is there a fallback value"
                      Just blob -> case Yaml.decodeEither' blob of
                        Right v1 -> pure v1
                        Left err ->
                          pure $
                            error $
                              "Could not read field "
                                ++ show fp
                                ++ "; got "
                                ++ show err
                )
        )
        (fail ("invalid version " ++ show cur))
    Nothing -> fail ("could not find version at commit " ++ show oid)

oidToCommit :: HasGitOid c => Store s -> c -> IO Git.Commit
oidToCommit s c = withStoreGit s (\pgit -> Git.lookupCommit pgit =<< gitOid c)

commits ::
  HasGitOid c => Store s -> Maybe Int -> Maybe c -> IO (V.Vector Git.Commit)
commits s sz mstart =
  withStoreGit s $ \pgit ->
    traverse gitOid mstart >>= \start ->
      Git.commits pgit Git.SortTimeTopo start (maybe (Git.Min 128) Git.Max sz)

--------------------------------------------------------------------------------

-- | Compute a git commit message given the revision/change number, the store
-- itself (used to get the store schema version), and a list of git signatures
-- which will be displayed in order on the commit.
commitMsg :: Stored s => Word -> store s -> [GitSignature] -> Text
commitMsg ch s others =
  T.concat $ T.pack (show (version s)) : "#" : T.pack (show ch) : contributions
  where
    contributor Git.GitSignature {gitSigName = name, gitSigEmail = email} =
      [" - ", name, " <", email, ">\n"]
    contributions = case others of
      [] -> []
      _ -> "\n\nContributors:\n" : concatMap contributor others

--------------------------------------------------------------------------------

newtype StoreJson a = StoreJson {getStoreJson :: a}

instance Stored a => ToJSON (StoreJson a) where
  {-# INLINE toJSON #-}
  toJSON (StoreJson s) =
    Object $! (`execState` H.empty) $
      (`withFields` s) $ \(FieldPath fp) x ->
        modify' (H.insert (T.pack fp) (toJSON x))

instance Stored a => FromJSON (StoreJson a) where
  {-# INLINE parseJSON #-}
  parseJSON (Object obj) = fmap StoreJson $
    construct $ \(FieldPath fp) _ ->
      case H.lookup (T.pack fp) obj of
        Just fld -> parseJSON fld
        Nothing -> fail ("could not find field " ++ show fp)
  parseJSON _ = fail "StoredJson expected object"
