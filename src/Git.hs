{-# LANGUAGE BangPatterns               #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveDataTypeable         #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PatternSynonyms            #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE Rank2Types                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE ViewPatterns               #-}
module Git where
import           Control.Exception
import           Control.Monad.Cont      hiding ( when )
import           Data.Aeson                     ( FromJSON(..)
                                                , ToJSON(..)
                                                , Value(..)
                                                )
import qualified Data.Aeson                    as J
import           Data.Aeson.TH                  ( defaultOptions
                                                , deriveJSON
                                                )
import           Data.ByteString                ( ByteString )
import qualified Data.ByteString               as B
import           Data.Char                      ( isHexDigit )
import           Data.Coerce
import           Data.Functor.Const
import           Data.Functor.Identity
import           Data.Hashable
import           Data.IORef
import qualified Data.Map.Strict               as M
import           Data.Monoid
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import qualified Data.Text.Encoding            as T
import           Data.Time
import           Data.Time.Clock.POSIX
import           Data.Typeable
import           Data.Vector                    ( Vector )
import qualified Data.Vector                   as V
import qualified Data.Vector.Mutable           as VM
import qualified Data.Vector.Storable          as S
import           Foreign                 hiding ( newForeignPtr )
import           Foreign.C
import           Foreign.Concurrent             ( newForeignPtr )
import           Git.DebugFlag                  ( debugFlag )
import qualified Language.C.Inline             as C
import qualified Language.C.Inline.Context     as C
import qualified Language.C.Inline.Context.Text
                                               as C
import qualified Language.C.Inline.Util        as C
import qualified Language.C.Types              as C
import           System.IO.Unsafe               ( unsafeInterleaveIO
                                                , unsafePerformIO
                                                )

--------------------------------------------------------------------------------
-- Supported git types

data GitRepository
data GitCommit
data GitTree
data GitIndex

-- | See libgit2/types.h
--
-- This is valid for only POSIX
newtype GitTime = GitTime Int64
  deriving newtype (Eq, Ord, Show, Num, Enum, Integral, Real, Bounded, Storable)

data GitSignature = GitSignature
  { gitSigName :: !Text
  , gitSigEmail :: !Text
  , gitSigWhen :: !(Maybe UTCTime)
  }
  deriving (Show, Eq, Ord)

instance Hashable GitSignature where
  hashWithSalt s GitSignature { gitSigName, gitSigEmail, gitSigWhen } =
    s
      `hashWithSalt` gitSigName
      `hashWithSalt` gitSigEmail
      `hashWithSalt` fmap (toRational . utcTimeToPOSIXSeconds) gitSigWhen

deriveJSON defaultOptions ''GitSignature

newtype GitOid = GitOid ByteString
  deriving newtype (Eq, Ord, Show)

newtype GitOidHash = GitOidHash ByteString
  deriving newtype (Eq, Ord, Show, Hashable)

parseGitOidHash :: Text -> Maybe GitOidHash
parseGitOidHash t
  | T.all isHexDigit t && T.length t <= 40 = Just (GitOidHash (T.encodeUtf8 t))
  | otherwise = Nothing

instance FromJSON GitOidHash where
  parseJSON v = case v of
    String t | T.all isHexDigit t && T.length t <= 40 ->
      return (GitOidHash (T.encodeUtf8 t))
    _ -> fail "expected git hash"

instance ToJSON GitOidHash where
  toJSON (GitOidHash h) = String (T.decodeUtf8 h)

newtype GitCommitOid = GitCommitOid GitOid
  deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

let
  gitCtx :: C.Context
  gitCtx = mempty
    { C.ctxTypesTable = M.fromList
      [ (C.TypeName "git_repository", [t| GitRepository |])
      , (C.TypeName "git_commit", [t| GitCommit |])
      , (C.TypeName "git_tree", [t| GitTree |])
      , (C.TypeName "git_index", [t| GitIndex |])
      , (C.TypeName "git_signature", [t| GitSignature |])
      , (C.TypeName "git_oid", [t| GitOid |])
      , (C.TypeName "git_time_t", [t| GitTime |])
      ]
    , C.ctxAntiQuoters = M.fromList
      [ C.newWithTyName "git_signature_now" "git_signature"
        [|withGitSignature . unsetGitSigWhen|]
        [t|GitSignature|]
      , C.new "git_signature" [|withGitSignature|] [t|GitSignature|]
      ]
    }
  in
  C.context (C.baseCtx <> C.bsCtx <> C.utf8Ctx <> C.fptrCtx <> C.vecCtx <>
             C.funCtx <> gitCtx)

-- Needed for GHCi. Not sure why, but the record update causes a syntax error
-- with GHCi 9+. This works around that.
unsetGitSigWhen :: GitSignature -> GitSignature
unsetGitSigWhen g = g { gitSigWhen = Nothing }

C.include "git2.h"
C.include "<stdio.h>"
C.include "<string.h>"
C.include "<stdalign.h>"
C.include "<sys/queue.h>"

debugFlag

C.verbatim "#define xstr(a) str(a)"
C.verbatim "#define str(a) #a"

C.verbatim "#define GIT_FAIL(X) \
           \  TRACEF(\"git failure: \" str(X) \" with %s\", giterr_last()->message)"

C.verbatim "#define GUARD(X) \
           \  do {\
           \    if((rc = (X)) != GIT_OK) {\
           \      GIT_FAIL(X);\
           \      return rc;\
           \    } else {\
           \      TRACE(str(X));\
           \    }\
           \  } while (0)"

C.verbatim "#define GUARDS(N, X) \
           \  do {\
           \    int ok = 0;\
           \    for (int errc = 0; errc < N; errc ++) {\
           \      printf(str(X) \"\\n\");\
           \      rc = (X);\
           \      if (rc == GIT_OK) { ok = 1; break; }\
           \      printf(str(X) \" failed attempt %d with %d\\n\", errc, rc);\
           \    }\
           \    if (!ok) {\
           \      printf(str(X) \" failed all attempts with %d\\n\", rc);\
           \      return rc;\
           \    }\
           \  } while (0)"

C.verbatim "#define TRACEF(X, ...) \
           \  do { if (DEBUG) { printf(X \"\\n\", __VA_ARGS__); } break; } while (1)"

C.verbatim "#define TRACE(X) \
           \  do { if (DEBUG) { printf(X \"\\n\"); } break; } while (1)"

C.verbatim "#define member_size(type, member) sizeof(((type *)0)->member)"

C.verbatim "typedef struct {\
           \  char* path;\
           \  int pathlen;\
           \  char* content;\
           \  int contentlen;\
           \} commit_fold_step;"


--------------------------------------------------------------------------------

{-# NOINLINE hasGitInit #-}
hasGitInit :: IORef Bool
hasGitInit = unsafePerformIO (newIORef False)

ensureGitInit :: IO ()
ensureGitInit = do
  ok <- readIORef hasGitInit
  unless ok gitInit

-- | This MUST be called (preferably via 'withGit') before using any other
-- function here or you WILL get unexpected segfaults!
gitInit :: IO ()
gitInit = do
  [C.block|
void {
#if LIBGIT2_VER_MAJOR == 0 && LIBGIT2_VER_MINOR > 21 || LIBGIT2_VER_MAJOR >= 1
  git_libgit2_init();
#else
  git_threads_init();
#endif
}|]
  writeIORef hasGitInit True

gitShutdown :: IO ()
gitShutdown = do
  [C.block|
    void {
      git_libgit2_shutdown();
    }|]
  writeIORef hasGitInit False

withGit :: IO a -> IO a
withGit fn = do
  gitInit
  r <- fn
  gitShutdown
  return r

--------------------------------------------------------------------------------

formatGitOid :: Coercible a GitOid => a -> IO GitOidHash
formatGitOid (coerce -> GitOid oid') = allocaBytes sz $ \out -> do
  let oid | B.length oid' < 20 = oid' <> B.replicate (20 - B.length oid') 0
          | otherwise = oid'
  [C.block|void {
    const char *input = $bs-ptr:oid;
    git_oid oid;
    git_oid_fromraw(&oid, input);
    git_oid_fmt($(char* out), &oid);
  }|]
  GitOidHash <$> B.packCStringLen (out, sz)
  where sz = 40

--------------------------------------------------------------------------------
-- Exception handling

newtype GitException = GitException CInt
  deriving stock (Show, Typeable)

instance Exception GitException

gitOkay :: GitException
gitOkay = GitException [C.pure|int { GIT_OK }|]

throwGit :: (Show a, Integral a) => IO a -> IO ()
throwGit a = a >>= \r -> unless (fromIntegral r == [C.pure|int { GIT_OK }|])
                                (throw (GitException (fromIntegral r)))

--------------------------------------------------------------------------------
-- Signatures

readGitSignature :: ForeignPtr GitSignature -> IO GitSignature
readGitSignature = flip withForeignPtr peekGitSignature

peekGitSignature :: Ptr GitSignature -> IO GitSignature
peekGitSignature inp = do
  -- this might invoke some compatability problems with C compilers that don't
  -- support at least C99 and thus don't have inttypes.h, which apparently still
  -- includes MSVC
  name <-
    fmap T.decodeUtf8
    . B.packCString
    =<< [C.exp|char* { $(git_signature* inp)->name } |]
  email <-
    fmap T.decodeUtf8
    . B.packCString
    =<< [C.exp|char* { $(git_signature* inp)->email } |]
  when <-
    Just
    . posixSecondsToUTCTime
    . fromIntegral
    <$> [C.exp|git_time_t { $(git_signature* inp)->when.time } |]
  return GitSignature { gitSigName = name
                      , gitSigEmail = email
                      , gitSigWhen = when
                      }

newGitSignature :: GitSignature -> IO (ForeignPtr GitSignature)
newGitSignature GitSignature { gitSigName = name, gitSigEmail = email, gitSigWhen = when }
  = C.foreignPtr
    (\out -> do
      t <- maybe getCurrentTime pure when
      tz <- getTimeZone t
      let time = floor (utcTimeToPOSIXSeconds t)
      let offset = fromIntegral (timeZoneMinutes tz)
      throwGit [C.block|int {
        int rc;
        git_signature** out = $(git_signature** out);
        char* name = $utf8:name;
        char* email = $utf8:email;
        int64_t time = $(int64_t time);
        int offset = $(int offset);
        GUARD(git_signature_new(out, name, email, time, offset));
        return rc;
      } |]
    )
    (\p -> [C.block|void {
        git_signature_free($(git_signature* p)); }|]
    )

withGitSignature :: GitSignature -> (Ptr GitSignature -> IO a) -> IO a
withGitSignature gs f = do
  c <- newGitSignature gs
  r <- withForeignPtr c f
  finalizeForeignPtr c
  return r

--------------------------------------------------------------------------------
-- Git repository initialisation

-- | Either load an existing repository or initialise a new one given some path
newGitRepository :: FilePath -> GitSignature -> IO (ForeignPtr GitRepository)
newGitRepository path gsig = C.foreignPtr
  (\out -> throwGit [C.block|int {
    int rc;
    git_repository** repo = $(git_repository** out);
    git_index* index;
    git_oid tree_id;
    git_oid commit_id;
    git_tree* tree;
    git_signature* author_sig = $git_signature:gsig;
    git_signature* commit_sig = $git_signature_now:gsig;

    char* path = $utf8[]:path;
    // if there is already a git repository here, just open it and be done
    if (git_repository_open(repo, path) == 0) {
      return 0;
    }
    GUARD(git_repository_init(repo, path, 0));
    GUARD(git_repository_index(&index, *repo));
    GUARD(git_index_write_tree(&tree_id, index));
    git_index_free(index);

    GUARD(git_tree_lookup(&tree, *repo, &tree_id));
    GUARD(git_commit_create_v(&commit_id, *repo, "HEAD", author_sig, commit_sig,
                              "UTF-8", "Initial commit", tree, 0));

    git_tree_free(tree);
    return 0; }|]
  )
  (\repo -> [C.block|
    void { git_repository_free($(git_repository* repo)); }|]
  )

--------------------------------------------------------------------------------
-- Commits

data GitAddMode
  = Glob
  | Exact
  deriving (Show, Read, Eq)

-- | Commit everything
commitAll
  :: ForeignPtr GitRepository -> Text -> GitSignature -> IO GitCommitOid
commitAll = commit_ Glob (V.fromList ["*"])

-- | Commit some paths exactly.
commit
  :: Vector FilePath
  -> ForeignPtr GitRepository
  -> Text
  -> GitSignature
  -> IO GitCommitOid
commit = commit_ Exact

commit_
  :: GitAddMode
  -> Vector FilePath
  -> ForeignPtr GitRepository
  -> Text
  -> GitSignature
  -> IO GitCommitOid
commit_ mode paths git msg gsig = (`runContT` return) $ do
  cpaths :: S.Vector CString <-
    S.convert <$> V.mapM (ContT . B.useAsCString . T.encodeUtf8 . T.pack) paths

  let addflags = case mode of
        Glob -> [C.pure|int { GIT_INDEX_ADD_DEFAULT }|]
        Exact -> [C.pure|int { GIT_INDEX_ADD_DISABLE_PATHSPEC_MATCH }|]

  liftIO $ alloca $ \out -> do
    throwGit [C.block|int {
      int rc;
      git_repository* repo = $fptr-ptr:(git_repository* git);
      git_signature* commit_sig = $git_signature_now:gsig;
      git_signature* author_sig = commit_sig;
      git_index* index;
      git_tree* tree;
      git_oid tree_id;
      git_oid out;
      git_oid parent_id;
      git_commit* parent;

      // get repository index
      GUARD(git_repository_index(&index, repo));
      const git_strarray pathspec = {
        .strings = $vec-ptr:(char** cpaths),
        .count = $vec-len:cpaths,
      };

      GUARD(git_index_add_all(index,
                              &pathspec,
                              $(int addflags),
                              NULL,
                              NULL));

      GUARD(git_index_write_tree(&tree_id, index));
      GUARD(git_tree_lookup(&tree, repo, &tree_id));

      // find last commit
      GUARD(git_reference_name_to_id(&parent_id, repo, "HEAD"));

      GUARD(git_commit_lookup(&parent, repo, &parent_id));

      if (parent == NULL) { return -1; }

      GUARD(git_commit_create_v(
        &out, repo, "HEAD", author_sig, commit_sig, "UTF-8", $utf8:msg, tree, 1,
        parent));

      memcpy($(char* out), out.id, sizeof(out.id));

      git_tree_free(tree);
      GUARD(git_index_write(index));
      git_index_free(index);
      git_commit_free(parent);
      return 0;
    }|]
    GitCommitOid <$> packOid out

data Commit = Commit
  { commitOid :: {-# UNPACK #-} !GitCommitOid
  , commitMessage :: {-# UNPACK #-} !Text
  , commitTime :: {-# UNPACK #-} !UTCTime
  , commitAuthor :: {-# UNPACK #-} !GitSignature
  , commitHash :: GitOidHash -- ^ lazily evaluated using 'unsafeInterleaveIO'
  }

instance ToJSON Commit where
  toJSON Commit {..} = J.object
    [ "oid" J..= commitHash
    , "time" J..= commitTime
    , "author" J..= J.object
      [ "name" J..= gitSigName commitAuthor
      , "email" J..= gitSigEmail commitAuthor
      ]
    , "message" J..= commitMessage
    ]

instance Show Commit where
  show Commit { commitHash, commitTime, commitMessage } = T.unpack $! T.concat
    [ T.decodeUtf8 (coerce commitHash)
    , " -- "
    , T.pack (show commitTime)
    , " -- "
    , T.replace "\n" "\\n" commitMessage
    ]

-- | Pack a 'CString' into a 'GitOid'.
--
-- Use this instead of 'B.packCString' -- a 0 is perfectly valid in a git_oid,
-- but will be seen as a null-terminator by 'B.packCString'
packOid :: CString -> IO GitOid
packOid ptr = GitOid <$> B.packCStringLen (ptr, fromIntegral sz)
  where sz = [C.pure|int { member_size(git_oid, id) }|]

-- | Construct a 'Commit' from the relevant C parts.
mkCommit
  :: C.CArray CUChar -- ^ OID
  -> CString -- ^ Message
  -> GitTime -- ^ git_commit_time
  -> Ptr GitSignature -- ^ git_commit_author
  -> IO Commit
mkCommit coid cmsg ctime gsig = do
  commitOid <- GitCommitOid <$> packOid (castPtr coid)
  commitMessage <- T.decodeUtf8 <$> B.packCString cmsg
  commitAuthor <- peekGitSignature gsig
  mkHash <- unsafeInterleaveIO (formatGitOid commitOid)
  return Commit { commitOid
                , commitMessage
                , commitTime = posixSecondsToUTCTime (fromIntegral ctime)
                , commitAuthor
                , commitHash = mkHash
                }

-- | Get the 'Commit' object corresponding to some 'GitOid'
lookupCommit
  :: Coercible a GitOid => ForeignPtr GitRepository -> a -> IO Commit
lookupCommit repo (coerce -> GitOid oid) = do
  cref <- newIORef (error "could not find HEAD commit")
  let setCommit coid cmsg ctime gsig =
        mkCommit coid cmsg ctime gsig >>= writeIORef cref
  throwGit [C.block|int {
    int rc = 0;
    git_oid oid;
    git_commit* commit = NULL;
    git_oid_fromraw(&oid, $bs-ptr:oid);
    git_repository* repo = $fptr-ptr:(git_repository* repo);
    GUARD(git_commit_lookup(&commit, $fptr-ptr:(git_repository* repo), &oid));
    if (commit == NULL) { return -1; }
    $fun:(void (*setCommit)(const unsigned char*,
                            const char*, const git_time_t,
                            const git_signature*))(
      oid.id,
      git_commit_message(commit),
      git_commit_time(commit),
      git_commit_author(commit));
    git_commit_free(commit);
    return rc;
  }|]
  readIORef cref

-- | Find the head commit.
headCommit :: ForeignPtr GitRepository -> IO Commit
headCommit repo = do
  cref <- newIORef (error "could not find HEAD commit")
  let setCommit coid cmsg ctime gsig =
        mkCommit coid cmsg ctime gsig >>= writeIORef cref
  throwGit [C.block|int {
    int rc;
    git_repository* repo = $fptr-ptr:(git_repository* repo);
    git_commit* commit = NULL;
    git_oid commit_id;
    GUARD(git_reference_name_to_id(&commit_id, repo, "HEAD"));
    GUARD(git_commit_lookup(&commit, repo, &commit_id));
    if (commit == NULL) { return -1; }
    $fun:(void (*setCommit)(const unsigned char*,
                            const char*, const git_time_t,
                            const git_signature*))(
      commit_id.id,
      git_commit_message(commit),
      git_commit_time(commit),
      git_commit_author(commit));
    git_commit_free(commit);
    return rc;
  }|]
  readIORef cref

--------------------------------------------------------------------------------
-- git log

data GitSort
  = SortNone
  | SortTopo
  | SortTimeTopo
  | SortTime
  | SortReverseNone
  | SortReverseTopo
  | SortReverseTime
  | SortReverseTimeTopo

fromGitSort :: GitSort -> CInt
fromGitSort gs0 = case gs0 of
  SortNone -> [C.pure|int{GIT_SORT_NONE}|]
  SortTopo -> [C.pure|int{GIT_SORT_TOPOLOGICAL}|]
  SortTimeTopo -> [C.pure|int{GIT_SORT_TIME | GIT_SORT_TOPOLOGICAL}|]
  SortTime -> [C.pure|int{GIT_SORT_TIME}|]
  SortReverseNone -> [C.pure|int{GIT_SORT_REVERSE | GIT_SORT_NONE}|]
  SortReverseTopo -> [C.pure|int{GIT_SORT_REVERSE | GIT_SORT_TOPOLOGICAL}|]
  SortReverseTime -> [C.pure|int{GIT_SORT_REVERSE | GIT_SORT_TIME}|]
  SortReverseTimeTopo ->
    [C.pure|int{GIT_SORT_REVERSE | GIT_SORT_TIME | GIT_SORT_TOPOLOGICAL}|]

data CommitListSizeHint
  = Max {-# UNPACK #-} !Int
  | Min {-# UNPACK #-} !Int

packGitOidHash :: GitOidHash -> IO GitOid
packGitOidHash (GitOidHash sha) = do
  out <- newIORef undefined
  let output :: CString -> IO ()
      output = writeIORef out <=< packOid
  [C.block|void {
    git_oid* oid = malloc(sizeof(git_oid));
    git_oid_fromstrn(oid, $bs-ptr:sha, $bs-len:sha);
    $fun:(void (*output)(char*))(oid->id);
    free(oid);
  }|]
  readIORef out

-- | List of up to N commits. Discards the initial commit.
commits
  :: ForeignPtr GitRepository
  -> GitSort
  -> Maybe GitOid
  -> CommitListSizeHint
  -> IO (Vector Commit)
commits repo gsort startCommitOid szhint = do
  (addOid, readVec) :: ( C.CArray CUChar     -- OID
    ->          -- 1: continue; 0: stop
       CString          -- Message
    -> GitTime          -- git_commit_time
    -> Ptr
         GitSignature -- git_commit_author
    -> CInt             -- current index
    -> IO
         CInt
    , Int -> IO (Vector Commit) -- finalize the vector given the final size
    ) <-
    case szhint of

      Max sz -> do
        -- if there is a maximum size hint, then we just make a vector and write
        -- git commits to it
        vec <- VM.new sz
        let addOid coid cmsg ctime gsig (fromIntegral -> ix) =
              if ix < VM.length vec
                then do
                  VM.write vec ix =<< mkCommit coid cmsg ctime gsig
                  return 1
                else return 0
        return (addOid, \l -> V.unsafeFreeze (VM.take l vec))

      Min sz -> do
        -- if there is a minimum size hint, grow the vector exponentially as
        -- needed to fit more commits
        vecref <- newIORef =<< VM.new sz
        let addOid coid cmsg ctime gsig (fromIntegral -> ix) = do
              vec <- readIORef vecref
              if ix < VM.length vec
                then VM.write vec ix =<< mkCommit coid cmsg ctime gsig
                else do
                  next <- VM.grow vec (VM.length vec)
                  VM.write next ix =<< mkCommit coid cmsg ctime gsig
                  writeIORef vecref next
              return 1
        return (addOid, \l -> V.unsafeFreeze . VM.take l =<< readIORef vecref)

  let !sort = fromGitSort gsort

  sz <- with (0 :: CInt) $ \sz ->
    with (nullPtr :: CString) $ \startCommitOidPtr -> (`runContT` return) $ do
    -- we store the start commit in Ptr CString
    -- if the Ptr points to null, we got Nothing; no start commit
    -- if it points to something else, it points to the CString for that git oid

      forM_ startCommitOid
        $ ContT
        . B.useAsCString
        . coerce
        >=> liftIO
        . poke startCommitOidPtr

      liftIO
        $ throwGit [C.block|int {
      int rc = 0;
      git_repository* repo = $fptr-ptr:(git_repository* repo);
      git_revwalk* walk = NULL;
      GUARD(git_revwalk_new(&walk, repo));

      char** start = $(char** startCommitOidPtr);
      if (*start == NULL) {
        GUARD(git_revwalk_push_head(walk));
      } else {
        git_oid start_oid;
        git_oid_fromraw(&start_oid, *start);
        GUARD(git_revwalk_push(walk, &start_oid));
      }

      git_revwalk_sorting(walk, $(int sort));

      // set up pathspec
      const char* path_strings[] = {"*"};
      const git_strarray paths = {
        .strings = (char**)path_strings,
        .count = 1,
      };
      git_pathspec* pathspec = NULL;
      git_pathspec_new(&pathspec, &paths);
      git_oid oid;
      git_commit* commit;
      int vm_ix = 0;
      while (git_revwalk_next(&oid, walk) == 0) {
        GUARD(git_commit_lookup(&commit, repo, &oid));
        if (git_commit_parentcount(commit) != 0) {
          if ($fun:(int (*addOid)(const unsigned char*, const char*,
                                  const git_time_t, const git_signature*,
                                  const int ix))(
              oid.id,
              git_commit_message(commit),
              git_commit_time(commit),
              git_commit_author(commit),
              vm_ix)) {
            vm_ix ++;
          } else {
            break;
          }
        }
        git_commit_free(commit);
      }
      *$(int* sz) = vm_ix;
      return rc;
    }|]
        >> peek sz

  readVec (fromIntegral sz)

{-# NOINLINE foldCommitWith #-}
-- | Fold over /all/ the files in a git repository at a certain commit.
foldCommitWith
  :: Coercible oid GitOid
  => oid
  -> ForeignPtr GitRepository
  -> (CString -> CString -> CInt -> IO CInt) -- ^ Raw callback. Return a
                                             -- negative value to terminate the
                                             -- walk Return a positive value to
                                             -- skip the passed entry
  -> IO ()
foldCommitWith (coerce -> GitOid oid) repo addPath = throwGit [C.block|int {
  int rc;
  git_repository* repo = $fptr-ptr:(git_repository* repo);
  git_oid commit_oid;
  git_oid_fromraw(&commit_oid, $bs-ptr:oid);
  git_commit* commit;
  GUARD(git_commit_lookup(&commit, repo, &commit_oid));
  git_tree* tree;
  GUARD(git_commit_tree(&tree, commit));
  int step(const char* root, const git_tree_entry* entry, void* payload) {
    int r;
    if (git_tree_entry_type(entry) == GIT_OBJ_BLOB) {
      const git_oid oid = *git_tree_entry_id(entry);
      git_blob* blob;
      GUARD(git_blob_lookup(&blob, repo, &oid));
      r = $fun:(int (*addPath)(const char*, const char*, int len))(
        git_tree_entry_name(entry),
        (char*) git_blob_rawcontent(blob),
        git_blob_rawsize(blob));
      git_blob_free(blob);
    }
    return r;
  }
  git_tree_walk(tree, GIT_TREEWALK_PRE, step, NULL); }|]

{-# NOINLINE foldCommitMay #-}
foldCommitMay
  :: Coercible oid ByteString
  => oid
  -> ForeignPtr GitRepository
  -> (String -> ByteString -> b -> IO (Maybe b))
  -> b
  -> IO b
foldCommitMay oid repo step z = do
  stuff <- newIORef z
  let addPath :: CString -> CString -> CInt -> IO CInt
      addPath fp content len = do
        fp' <- B.packCString fp
        content' <- B.packCStringLen (content, fromIntegral len)
        r <- step (T.unpack (T.decodeUtf8 fp')) content' =<< readIORef stuff
        case r of
          Just x -> do
            writeIORef stuff $! x
            return 0
          Nothing -> return 1
  foldCommitWith oid repo addPath
  readIORef stuff

foldCommit
  :: Coercible oid ByteString
  => oid
  -> ForeignPtr GitRepository
  -> (String -> ByteString -> b -> IO b)
  -> b
  -> IO b
foldCommit oid repo step =
  foldCommitMay oid repo (\fp bs acc -> Just <$> step fp bs acc)

