{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
module Git.DebugFlag (debugFlag) where
import qualified Language.C.Inline as C

#ifndef HOARDINGS_GIT_DEBUG
debugFlag = C.verbatim "#define DEBUG 0"
#else
debugFlag = C.verbatim "#define DEBUG 1"
#endif
