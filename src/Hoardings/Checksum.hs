{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Hoardings.Checksum where

import Control.DeepSeq (NFData (rnf))
import Control.Lens
import Control.Monad.ST.Strict
import Data.Bits
import qualified Data.ByteString as B
import Data.Functor.Classes
import Data.Int
import Data.Ratio (Ratio, denominator, numerator)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Vector as V
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Storable.Mutable as VSM
import qualified Data.Vector.Unboxed as VU
import Data.Word (Word16, Word32, Word64, Word8)
import Foreign (Storable)
import GHC.Generics
import qualified Hoardings.Adler32 as A32
import Text.Printf (printf)

--------------------------------------------------------------------------------

newtype Checksum = Checksum A32.RollingAdler32
  deriving (Eq)

checksumToWord64 :: Checksum -> Word64
checksumToWord64 (Checksum A32.RollingAdler32 {a, b}) =
  shiftL (fromIntegral a) 32 .|. fromIntegral b

word64ToChecksum :: Word64 -> Checksum
word64ToChecksum w = Checksum A32.RollingAdler32 {a = fromIntegral (w `shiftR` 32), b = fromIntegral w}

instance Show Checksum where
  show (Checksum r) = printf "0x%04x" (fromIntegral (A32.a r) .|. fromIntegral (A32.b r) `shiftR` 32 :: Word64)

class Checksumable a where
  feed :: a -> A32.Adler32Update (ST s)
  default feed :: (Generic a, GFeed (Rep a)) => a -> A32.Adler32Update (ST s)
  feed = gfeed . GHC.Generics.from

  checksum :: a -> Checksum
  checksum f = Checksum (A32.runST (feed f))

feedStorable :: Storable a => a -> A32.Adler32Update (ST s)
feedStorable a = A32.feedBuffer (VS.unsafeCast (VS.singleton a) :: VS.Vector Word8)

feedIntegral :: Integral a => a -> A32.Adler32Update (ST s)
feedIntegral a = A32.feedInteger (toInteger a)

instance Checksumable () where
  feed _ = A32.feedWord8 0

instance Checksumable a => Checksumable (Maybe a) where
  feed Nothing = A32.feedWord8 0
  feed (Just x) = A32.feedWord8 1 >> feed x

instance (Checksumable a, Checksumable b) => Checksumable (Either a b) where
  feed (Left a) = A32.feedWord8 0 >> feed a
  feed (Right b) = A32.feedWord8 1 >> feed b

instance (Checksumable a, Checksumable b) => Checksumable (a, b) where
  feed (a, b) = do
    feed a
    feed b

instance Checksumable Word where
  feed = A32.feedWord

instance Checksumable Word8 where
  feed = A32.feedWord8

instance Checksumable Word16 where
  feed = A32.feedWord16

instance Checksumable Word32 where
  feed = A32.feedWord32

instance Checksumable Word64 where
  feed = A32.feedWord64

instance Checksumable Int where
  feed = A32.feedInt

instance Checksumable Integer where
  feed = A32.feedInteger

instance Checksumable Int8 where
  feed = A32.feedWord8 . fromIntegral

instance Checksumable Int16 where
  feed = A32.feedWord16 . fromIntegral

instance Checksumable Int32 where
  feed = A32.feedWord32 . fromIntegral

instance Checksumable Int64 where
  feed = A32.feedWord64 . fromIntegral

instance Checksumable B.ByteString where
  feed = A32.feedBuffer

instance Checksumable Char where
  feed = A32.feedInt . fromEnum

instance Checksumable T.Text where
  feed = T.foldr (\x acc -> feed x >> acc) (pure ())

instance Checksumable a => Checksumable [a] where
  feed = feed

instance Checksumable a => Checksumable (Ratio a) where
  feed a = do
    feed (numerator a)
    feed (denominator a)

instance Checksumable a => Checksumable (V.Vector a) where
  feed = V.mapM_ feed

instance (VU.Unbox a, Checksumable a) => Checksumable (VU.Vector a) where
  feed = VU.mapM_ feed

instance Storable a => Checksumable (VS.Vector a) where
  feed = A32.feedBuffer . VS.unsafeCast

class GFeed f where
  gfeed :: f x -> A32.Adler32Update (ST s)

instance GFeed V1 where
  {-# INLINE gfeed #-}
  gfeed _ = pure ()

instance GFeed U1 where
  {-# INLINE gfeed #-}
  gfeed _ = A32.feedWord8 0

instance (GFeed f, GFeed g) => GFeed (f :+: g) where
  {-# INLINE gfeed #-}
  gfeed (L1 a) = A32.feedWord8 0 >> gfeed a
  gfeed (R1 b) = A32.feedWord8 0 >> gfeed b

instance (GFeed f, GFeed g) => GFeed (f :*: g) where
  {-# INLINE gfeed #-}
  gfeed (f :*: g) = gfeed f >> gfeed g

instance Checksumable c => GFeed (K1 i c) where
  {-# INLINE gfeed #-}
  gfeed (K1 c) = feed c

instance GFeed f => GFeed (M1 i t f) where
  {-# INLINE gfeed #-}
  gfeed (M1 fp) = gfeed fp

--------------------------------------------------------------------------------
