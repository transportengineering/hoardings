{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}

module Adler32 (tests) where

import Data.Bits
import Data.List (unfoldr)
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Unboxed as VU
import Data.Word
import qualified Hoardings.Adler32 as A32
import Hoardings.Checksum (checksumToWord64)
import qualified Hoardings.Checksum as CS
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit
import Test.Tasty.SmallCheck
import Test.Tasty.TH

prop_feed_1 a =
  A32.run (A32.feedWord8 a) == A32.run (A32.feedBuffer (VU.fromList [a]))

prop_feed_many as =
  A32.run (mapM_ A32.feedWord8 as)
    == A32.run (A32.feedBuffer (VU.fromList as))

prop_feed_many_force_large as =
  A32.run (mapM_ A32.feedWord8 as)
    == A32.run (A32.feedLargeBuffer (VU.fromList as))

prop_feed_small_slow (VU.fromList -> vec) =
  A32.run (A32.feedSmallBuffer vec) == A32.run (A32.feedSlow vec)

prop_feed_large_slow (VU.fromList -> vec) =
  A32.run (A32.feedLargeBuffer vec) == A32.run (A32.feedSlow vec)

prop_feed_small_large (VU.fromList -> vec) =
  A32.run (A32.feedSmallBuffer vec) == A32.run (A32.feedLargeBuffer vec)

prop_feed_small_large_16 (VU.fromList -> vec) =
  A32.run (A32.feedSmallBuffer vec16) == A32.run (A32.feedLargeBuffer vec16)
  where
    vec16 = VS.unsafeCast (VU.convert (vec :: VU.Vector Word16)) :: VS.Vector Word8

prop_feed_small_large_32 (VU.fromList -> vec) =
  A32.run (A32.feedSmallBuffer vec32) == A32.run (A32.feedLargeBuffer vec32)
  where
    vec32 = VS.unsafeCast (VU.convert (vec :: VU.Vector Word32)) :: VS.Vector Word8

prop_storable_cast_a1 list =
  A32.run (mapM_ A32.feedWord64 list)
    == A32.run (VS.mapM_ A32.feedWord8 (VS.unsafeCast vec))
  where
    vec = VS.fromList list

prop_storable_cast_a2 list =
  A32.run (mapM_ A32.feedWord32 list)
    == A32.run (VS.mapM_ A32.feedWord8 (VS.unsafeCast vec))
  where
    vec = VS.fromList list

prop_storable_cast_a3 list =
  A32.run (mapM_ A32.feedWord16 list)
    == A32.run (VS.mapM_ A32.feedWord8 (VS.unsafeCast vec))
  where
    vec = VS.fromList list

prop_storable_cast_a4 list =
  A32.run (mapM_ A32.feedWord8 list)
    == A32.run (VS.mapM_ A32.feedWord8 (VS.unsafeCast vec))
  where
    vec = VS.fromList list

prop_storable_int_vs_native (int :: Int) =
  A32.run (A32.feedInt int) == A32.run (A32.feedBuffer (VS.unsafeCast (VS.singleton int)))

prop_word_same_as_int (int :: Int) =
  (int >= 0)
    ==> A32.run (A32.feedInt int) == A32.run (A32.feedWord (fromIntegral int))

prop_1_byte_checksums_differ (a :: Word8) (b :: Word8) =
  (a /= b)
    ==> A32.run (A32.feedWord8 a) /= A32.run (A32.feedWord8 b)

prop_2_bytes_checksums_differ (a :: Word16) (b :: Word16) =
  (a /= b)
    ==> A32.run (A32.feedWord16 a) /= A32.run (A32.feedWord16 b)

-- probably not true with exhaustive checks
prop_4_bytes_checksums_differ (a :: Word32) (b :: Word32) =
  (a /= b)
    ==> A32.run (A32.feedWord32 a) /= A32.run (A32.feedWord32 b)

prop_checksum_vs_word64 =
  changeDepth (+ 100) \(a :: Word64) ->
    CS.checksumToWord64 (CS.word64ToChecksum a) == a

--------------------------------------------------------------------------------

tests :: TestTree
tests = $(testGroupGenerator)
