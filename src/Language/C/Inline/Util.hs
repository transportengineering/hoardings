{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes        #-}
{-# LANGUAGE TemplateHaskell   #-}
module Language.C.Inline.Util where
import           Data.Monoid                         (mempty)
import           Data.Text                           (Text, pack)
import           Foreign                             (ForeignPtr, Ptr, alloca,
                                                      malloc, newForeignPtr_,
                                                      peek, nullPtr, poke)
import           Foreign.Concurrent                  (newForeignPtr)
import           Language.C.Inline.Context
import           Language.C.Inline.HaskellIdentifier
import           Language.C.Types
import qualified Language.Haskell.TH                 as TH

getHsVariable :: String -> HaskellIdentifier -> TH.ExpQ
getHsVariable err s = do
  mbHsName <- TH.lookupValueName $ unHaskellIdentifier s
  case mbHsName of
    Nothing -> fail $ "Cannot capture Haskell variable " ++ unHaskellIdentifier s ++
                      ", because it's not in scope. (" ++ err ++ ")"
    Just hsName -> TH.varE hsName

foreignPtr :: (Ptr (Ptr a) -> IO ())
           -> (Ptr a -> IO ())
           -> IO (ForeignPtr a)
foreignPtr alloc dealloc = alloca $ \p -> do
  poke p nullPtr
  alloc p
  inner <- peek p
  r <- newForeignPtr inner (dealloc inner)
  return r

foreignPtr_ :: (Ptr (Ptr a) -> IO ()) -> IO (ForeignPtr a)
foreignPtr_ alloc = alloca $ \p -> do
  poke p nullPtr
  alloc p
  inner <- peek p
  r <- newForeignPtr_ inner
  return r

new :: CIdentifier -> TH.ExpQ -> TH.TypeQ -> (String, SomeAntiQuoter)
new name = newWithTyName (unCIdentifier name) name

newWithTyName
  :: String
  -> CIdentifier
  -> TH.ExpQ
  -> TH.TypeQ
  -> (String, SomeAntiQuoter)
newWithTyName aqName name withExpr cTyExpr =
  (aqName, SomeAntiQuoter AntiQuoter
    { aqParser = do
        hId <- parseIdentifier
        let cId = mangleHaskellIdentifier True hId
        return (cId, Ptr [] (TypeSpecifier mempty (TypeName name)), hId)

    , aqMarshaller = \_purity _cTypes cTy cId -> case cTy of
        Ptr [] (TypeSpecifier _ (TypeName cname)) | cname == name -> do
          hsTy <- [t| Ptr $cTyExpr |]
          hsExp' <- [| $withExpr $(getHsVariable "simple" cId) |]
          return (hsTy, hsExp')
        _ ->
          fail $ "cannot convert for " ++ show name ++ ", " ++ show cTy
    })

