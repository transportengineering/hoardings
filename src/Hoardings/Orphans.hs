{-# OPTIONS_GHC -fno-warn-orphans #-}
module Hoardings.Orphans () where
import           Data.Aeson                (FromJSON (..), ToJSON (..))
import           Data.Hashable             (Hashable)
import qualified Data.Hashable             as SH (Hashed, hashed, unhashed)
import qualified Data.Hashable.Cached.Lazy as LH (Hashed, hashed, unhashed)

instance (Hashable a, FromJSON a) => FromJSON (SH.Hashed a) where
  parseJSON v = fmap SH.hashed (parseJSON v)

instance (Hashable a, ToJSON a) => ToJSON (SH.Hashed a) where
  toJSON = toJSON . SH.unhashed

instance (Hashable a, FromJSON a) => FromJSON (LH.Hashed a) where
  parseJSON v = fmap LH.hashed (parseJSON v)

instance (Hashable a, ToJSON a) => ToJSON (LH.Hashed a) where
  toJSON = toJSON . LH.unhashed


